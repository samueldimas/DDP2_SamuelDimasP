/**
 * Code Author (Mahasiswa)
 * @author Samuel Dimas Partogi, NPM 1706074915, Kelas DDP2-B, GitLab Account: samueldimas
 * Code Version: 1.2.0
 */

import java.util.ArrayList;

public class Manusia {

    private String nama;
    private int umur, uang;
    private float kebahagiaan;
    private boolean hidup;
    public static ArrayList<Manusia> semuaManusia = new ArrayList<>();

    public Manusia (String nama, int umur, int uang) {
        this.nama = nama;
        this.umur = umur;
        this.uang = uang;
        this.kebahagiaan = 50;
        this.hidup = true;
        semuaManusia.add(this);
    }

    public Manusia (String nama, int umur) {
        this.nama = nama;
        this.umur = umur;
        this.uang = 50000;
        this.kebahagiaan = 50;
        this.hidup = true;
        semuaManusia.add(this);
    }

    public void setNama (String nama) {
        this.nama = nama;
    }

    public void setUmur (int umur) {
        this.umur = umur;
    }

    public void setUang (int uang) {
        this.uang = uang;
    }   

    public void setKebahagiaan (float kebahagiaan) {
        if (kebahagiaan < 0) {
            this.kebahagiaan = 0;
        } else if (kebahagiaan > 100) {
            this.kebahagiaan = 100;
        } else {
            this.kebahagiaan = kebahagiaan;
        }
    }

    public void setHidup (boolean hidup) {
        this.hidup = hidup;
    }

    public String getNama() {
        return nama;
    }

    public int getUmur() {
        return umur;
    }

    public int getUang() {
        return uang;
    }

    public float getKebahagiaan() {
        return kebahagiaan;
    }

    public boolean getHidup() {
        return hidup;
    }

    public void beriUang (Manusia penerima) {
        if (this.hidup == false) {
            System.out.println(getNama() + " telah tiada.");
        } else if ((this.hidup == false) && (penerima.getHidup() == false)) {
            System.out.println("Keduanya telah mati.");
        } else {
            int ascii = 0;
        
            for (int i = 0; i < penerima.getNama().length(); i++) {
                ascii += (int)(penerima.getNama().charAt(i));  
            }
        
            int total = ascii * 100;
        
            if (getUang() > total) {
                setKebahagiaan(getKebahagiaan() + (float) (total / 6000.0));
                penerima.setKebahagiaan(penerima.getKebahagiaan() + (float) (total / 6000.0));
                setUang(getUang() - total);
                penerima.setUang(penerima.getUang() + total);
                System.out.println(getNama() + " memberi uang sebanyak " +  total +  " kepada " + penerima.getNama() + ", mereka berdua senang :D");
            } else {
                System.out.println(getNama() + " ingin memberi uang kepada " + penerima.getNama() + " namun tidak memiliki cukup uang :'(");
            }
        }            
    }
    
    public void beriUang (Manusia penerima, int jumlah) {
        if (this.hidup == false) {
            System.out.println(getNama() + " telah tiada.");
        } else if ((this.hidup == false) && (penerima.getHidup() == false)) {
            System.out.println("Keduanya telah mati.");
        } else {
            if (getUang() > jumlah) {
                setKebahagiaan(getKebahagiaan() + (float) (jumlah / 6000.0));
                penerima.setKebahagiaan(penerima.getKebahagiaan() + (float) (jumlah / 6000.0));
                setUang(getUang() - jumlah);
                penerima.setUang(penerima.getUang() + jumlah);
                System.out.println(getNama() + " memberi uang sebanyak " + jumlah +  " kepada " + penerima.getNama() + ", mereka berdua senang :D");
            } else {
                System.out.println(getNama() + " ingin memberi uang kepada " + penerima.getNama() + " namun tidak memiliki cukup uang :'(");
            } 
        }
    }
         
    
    public void bekerja(int durasi, int bebanKerja) {
        if (this.hidup == false) {
            System.out.println(getNama() + " telah tiada.");
        } else {
            if (getUmur() < 18) {
                System.out.println(getNama() + " belum boleh bekerja karena masih dibawah umur D:");
            } else {
                int bebanKerjaTotal = durasi * bebanKerja;
            
                if (bebanKerjaTotal <= getKebahagiaan()) {
                    setKebahagiaan(getKebahagiaan() - bebanKerjaTotal);
                    int pendapatan = bebanKerjaTotal * 10000;
                    System.out.println(getNama() + " bekerja full time, total pendapatan : " + pendapatan);
                    setUang(uang + pendapatan);
                } else {
                    int durasiBaru = (int) getKebahagiaan() / bebanKerja;
                    int bebanKerjaTotalBaru = durasiBaru * bebanKerja;
                    int pendapatan = bebanKerjaTotalBaru * 10000;
                    setKebahagiaan(getKebahagiaan() - bebanKerjaTotalBaru);
                    System.out.println(getNama() + " tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : " + pendapatan);
                    setUang(uang + pendapatan);
                }
            }
        }     
    }

    public void rekreasi (String namaTempat) {
        if (this.hidup == false) {
            System.out.println(getNama() + " telah tiada.");
        } else {
            int biaya = namaTempat.length() * 10000;
                
                if (getUang() > biaya) {
                    setKebahagiaan(getKebahagiaan() + namaTempat.length());
                    setUang(getUang() - biaya);
                    System.out.println(getNama() + " berekreasi di " + namaTempat + ", " + getNama() +  " senang :)");
                } else {
                    System.out.println(getNama() + " tidak mempunyai cukup uang untuk berekreasi di " + namaTempat + " :(");
                }
        }
    }

    public void sakit (String namaPenyakit) {
        if (this.hidup == false) {
            System.out.println(getNama() + " telah tiada.");
        } else {
            setKebahagiaan(getKebahagiaan() - namaPenyakit.length());
                System.out.println(getNama() + " terkena penyakit " + namaPenyakit + " :O");
        }
    }

    public String toString () {
        return "Nama\t\t: " + getNama() + "\nUmur\t\t: " + getUmur() + "\nUang\t\t: " + getUang() + "\nKebahagiaan\t: " + getKebahagiaan();
    }

    public void meninggal () {
        if (this.hidup == false) {
            System.out.println(getNama() + " telah tiada.");
        } else {
            for (int i = semuaManusia.size() - 1; i >= 0; i--) {
                if (semuaManusia.get(i).getHidup() == true) {
                    semuaManusia.get(i).setUang(getUang() + this.getUang());
                    setUang(0);
                    setHidup(false);
                    System.out.println(getNama() + " meninggal dengan tenang, kebahagiaan : " + getKebahagiaan());
                    System.out.println("Semua harta " + getNama() + " disumbangkan untuk " + getNama());
                    setNama("Almarhum " + getNama());
                    break;
                }
            }
        } 
    }
}



