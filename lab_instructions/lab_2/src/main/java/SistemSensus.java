import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author Samuel Dimas Partogi, NPM 1706074915, Kelas DDP2-B, GitLab Account: samueldimas
 */

public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);

		String nama, alamat, tanggalLahir, catatan;
		short panjang, lebar, tinggi;
		byte makanan, jumlahCetakan;
		float berat;

		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// User Interface untuk meminta masukan
		try {

			System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
					"--------------------\n" +
					"Nama Kepala Keluarga   : ");
			nama = input.nextLine();

			System.out.print("Alamat Rumah           : ");
			alamat = input.nextLine();

			System.out.print("Panjang Tubuh (cm)     : ");
			panjang = Short.parseShort(input.nextLine());

			System.out.print("Lebar Tubuh (cm)       : ");
			lebar = Short.parseShort(input.nextLine());

			System.out.print("Tinggi Tubuh (cm)      : ");
			tinggi = Short.parseShort(input.nextLine());

			System.out.print("Berat Tubuh (kg)       : ");
			berat = Float.parseFloat(input.nextLine());

			System.out.print("Jumlah Anggota Keluarga: ");
			makanan = Byte.parseByte(input.nextLine());

			System.out.print("Tanggal Lahir          : ");
			tanggalLahir = input.nextLine();
			
			System.out.print("Catatan Tambahan       : ");
			catatan = input.nextLine();

			System.out.print("Jumlah Cetakan Data    : ");
			jumlahCetakan = Byte.parseByte(input.nextLine());
		
		} catch (NumberFormatException num) {
			System.out.println("WARNING: Keluarga ini tidak perlu di relokasi!");
			return;
		}

		if (!(0 < panjang && panjang <= 250) ||
			!(0 < lebar && lebar <= 250) ||
			!(0 < tinggi && tinggi <= 250)||
			!(0 < berat && berat <= 150)||
			!(0 < makanan && makanan <= 20)) {
			System.out.println("WARNING: Keluarga ini tidak perlu di relokasi!");
			return;
		} 


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// TODO Hitung rasio berat per volume (rumus lihat soal)
		int rasio = (int) (berat * 1000000) / (panjang * lebar * tinggi);

		for (int i = 1; i <= jumlahCetakan; i++) {
			// TODO Minta masukan terkait nama penerima hasil cetak data
			System.out.print("Pencetakan " + i + " dari " + jumlahCetakan + " untuk: ");
			String penerima = input.nextLine().toUpperCase(); // Lakukan baca input lalu langsung jadikan uppercase

			// TODO Periksa ada catatan atau tidak
			String catatanBaru;

			if (catatan.isEmpty()) { 
				catatanBaru = "Tidak ada catatan tambahan";
			} else {
				catatanBaru = "Catatan: " + catatan;
			}
			
			// TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
			String hasil = "DATA SIAP DICETAK UNTUK " + penerima + "\n--------------------\n" + 
							nama + " - " + alamat + 
							"\nLahir pada tanggal " + tanggalLahir + 
							"\nRasio berat per volume     = " + rasio + " kg/m^3\n" + catatanBaru;
			System.out.println(hasil);
		}

		
		// TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
		// TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)
		short jumlahASCII = 0;

		for (int angka = 0; angka < nama.length(); angka++) jumlahASCII += (int) nama.charAt(angka);
		

		int kalkulasi = (int) (((panjang * lebar * tinggi) + jumlahASCII) % 10000);

		
		// TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		String nomorKeluarga = nama.substring(0,1) + String.valueOf(kalkulasi);

		// TODO Hitung anggaran makanan per tahun (rumus lihat soal)
		int anggaran = 50000 * 365 * (int) makanan;

		// TODO Hitung umur dari tanggalLahir (rumus lihat soal)
		short tahunLahir = Short.parseShort(tanggalLahir.substring(tanggalLahir.length() - 4)); 
		short umur = (short) (2018 - tahunLahir);

		// TODO Lakukan proses menentukan apartemen (kriteria lihat soal)
		String apartemen, kabupaten;

		if (0 <= umur && umur <= 18) {
			apartemen = "PPMT";
			kabupaten = "Rotunda";
		} else {
			if (0 <= anggaran && anggaran <= 100000000) {
				apartemen = "Teksas";
				kabupaten = "Sastra";
			} else {
				apartemen = "Mares";
				kabupaten = "Margonda";
			}
		}

		// TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
		String rekomendasi = "REKOMENDASI APARTEMEN" + 
							 "\n--------------------\n" + 
							 "MENGETAHUI: Identitas keluarga: " + nama + " - " + nomorKeluarga +
							 "\nMENIMBANG: Anggaran makanan tahunan: Rp " + anggaran + 
							 "            \nUmur kepala keluarga: " + umur + " tahun" + 
							 "\nMEMUTUSKAN: keluarga " + nama + " akan ditempatkan di:\n" + 
							 apartemen + ", kabupaten " + kabupaten;
		System.out.println(rekomendasi);
		
		input.close();
	}
}