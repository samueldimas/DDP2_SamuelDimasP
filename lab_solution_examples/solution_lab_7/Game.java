import character.*;
import java.util.ArrayList;

public class Game{
    ArrayList<Player> player = new ArrayList<Player>();
    public Player find(String name){
    for (int i = 0; i < this.player.size(); i++){
            if(this.player.get(i).getName().equals(name)){
                return this.player.get(i);
            }
        }
    return null;
    }
    public String add(String chara, String tipe, int hp){
        Player me = find(chara);
        if(me != null){
            return "Sudah ada karakter bernama " + chara;
        }
        if(tipe.equalsIgnoreCase("monster")){
            player.add(new Monster(chara, hp));
            return chara + " ditambah ke game";
        } else if(tipe.equalsIgnoreCase("magician")){
            player.add(new Magician(chara, hp));
            return chara + " ditambah ke game";
        } else if(tipe.equalsIgnoreCase("human")){
            player.add(new Human(chara, hp));
            return chara + " ditambah ke game";            
        }
        return "Tipe player harus monster, human atau magician";
    }
    public String add(String chara, String tipe, int hp, String roar){
        Player me = find(chara);
        if(me != null){
            return "Sudah ada karakter bernama " + me;
        } else if(tipe.equalsIgnoreCase("monster")){
            return "hanya monster yang boleh memiliki roar";
        }
        player.add(new Monster(chara, hp, roar));
        return chara + " ditambah ke game";
    }
    public String remove(String chara){
        Player me = find(chara);
        if(me == null){
            return "Tidak ada "+ chara;
        }
        player.remove(me);
        return chara + " dihapus dari game";
    }
    public String status(String chara){
        Player me = find(chara);
        if(me == null){
            return "Tidak ada "+ chara;
        }
        return me.status();
    }
    public String status(){
        String output = "";
        for (int i=0;i<player.size() ;i++) {
            output+=player.get(i).status() + "\n";
        }
        return output;        
    }
    public String diet(String chara){
        Player me = find(chara);
        if(me == null){
            return "Tidak ada "+ chara;
        }
        return me.diet();
    }
    public String diet(){
        String output = "";
        for (int i=0;i<player.size() ;i++) {
            output+=player.get(i).diet() + "\n";
        }
        return output;
    }
    public String attack(String meName, String enemyName){
        Player me = find(meName);
        Player enemy = find(enemyName);
        if(me == null || enemy == null){
            return "Tidak ada "+ meName + " atau " + enemyName;
        }
        return me.attack(enemy);
    }
    public String burn(String meName, String enemyName){
        Player me = find(meName);
        Player enemy = find(enemyName);
        if(me == null || enemy == null){
            return "Tidak ada "+ meName + " atau " + enemyName;
        }
        return ((Magician) me).burn(enemy);
    }
    public String eat(String meName, String enemyName){
        Player me = find(meName);
        Player enemy = find(enemyName);
        if(me == null || enemy == null){
            return "Tidak ada "+ meName + " atau " + enemyName;
        }
        if(me.canEat(enemy)){
            player.remove(enemy);
        }
        return me.eat(enemy);
    }
    public String roar(String meName){
        Player a = this.find(meName);
        if(a instanceof Monster){
            return ((Monster) a).roar();
        } else if(a == null){
            return "Tidak ada " + meName;
        }
        return meName + " tidak bisa berteriak";
    }
}