public class Manusia {
    private String nama;
    private int umur;
    private int uang;
    private float kebahagiaan;
    private boolean meninggal;

    private static final int KEBAHAGIAAN_AWAL = 50;
    private static final int JUMLAH_UANG_AWAL = 50000;
    private static final int MAX_KEBAHAGIAAN = 100;
    private static final int MIN_KEBAHAGIAAN = 0;
    private static final int UPAH_PER_BEBANKERJA = 10000;
    private static final int USIA_KERJA = 18;
    private static final int BIAYA_REKREASI_PER_KEBAHAGIAAN = 10000;

    private static Manusia manusiaTerakhir;

    public Manusia(String nama, int umur) {
        this.nama = nama;
        this.umur = umur;
        this.uang = JUMLAH_UANG_AWAL;
        this.kebahagiaan = KEBAHAGIAAN_AWAL;
        meninggal = false;
        manusiaTerakhir = this;
    }

    public Manusia(String nama, int umur, int uang) {
        this(nama, umur);
        this.uang = uang;
    }

    public void beriUang(Manusia other) {
        String namaPenerima = other.getNama();
        int len = namaPenerima.length();
        int jumlah = 0;
        for (int ii = 0; ii < len; ii++) {
            jumlah += namaPenerima.charAt(ii);
        }
        jumlah *= 100;

        beriUang(other, jumlah);
    }

    public void beriUang(Manusia other, int jumlah) {
        if (!meninggal) {
            if (!other.meninggal) {
                if (uang >= jumlah) {
                    uang -= jumlah;
                    other.uang += jumlah;

                    tambahKebahagiaan(jumlah / 6000.0);
                    other.tambahKebahagiaan(jumlah / 6000.0);

                    System.out.println(nama + " memberi uang sebanyak " + jumlah + " kepada " + other.nama + ", mereka berdua senang :D");
                } else {
                    System.out.println(nama + " ingin memberi uang kepada " + other.nama + " namun tidak memiliki cukup uang :'(");
                }
            }
            else {
                System.out.println(other.nama + " telah tiada");
            }
        }
        else {
            System.out.println(nama + " telah tiada");
        }
    }

    public void bekerja(int durasi, int bebanKerja) {
        if (!meninggal) {
            if (umur < USIA_KERJA) {
                System.out.println(nama + " belum boleh bekerja karena masih dibawah umur D:");
            } else {
                int bebanFullKerja = bebanKerja * durasi;
                if (bebanFullKerja <= kebahagiaan) {
                    kebahagiaan -= bebanFullKerja;
                    int pendapatan = bebanFullKerja * UPAH_PER_BEBANKERJA;
                    uang += pendapatan;
                    System.out.println(nama + " bekerja full time, total pendapatan : " + pendapatan);
                } else {
                    int durasiBekerja = (int) (kebahagiaan / bebanKerja);
                    int bebanHalfKerja = durasiBekerja * bebanKerja;
                    kebahagiaan -= bebanHalfKerja;
                    int pendapatan = bebanHalfKerja * UPAH_PER_BEBANKERJA;
                    uang += pendapatan;
                    System.out.println(nama + " tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : " + pendapatan);
                }
            }
        }
        else {
            System.out.println(nama + " telah tiada");
        }
    }

    public void rekreasi(String tempat) {
        if (!meninggal) {
            int biaya = tempat.length() * BIAYA_REKREASI_PER_KEBAHAGIAAN;
            if (uang >= biaya) {
                uang -= biaya;
                tambahKebahagiaan(tempat.length());
                System.out.println(nama + " berekreasi di " + tempat + ", " + nama + " senang :)");
            } else {
                System.out.println(nama + " tidak mempunyai cukup uang untuk berekreasi di " + tempat + " :(");
            }
        }
        else {
            System.out.println(nama + " telah tiada");
        }
    }

    public void sakit(String penyakit) {
        if (!meninggal) {
            kurangKebahagiaan(penyakit.length());
            System.out.println(nama + " terkena penyakit " + penyakit + " :O");
        }
        else {
            System.out.println(nama + " telah tiada");
        }
    }

    @Override
    public String toString() {
        String ret = "";
        if (!meninggal) ret += "Nama\t\t: " + nama;
        else ret += "Nama\t\t: Almarhum " + nama;
        ret += "\nUmur\t\t: " + umur;
        ret += "\nUang\t\t: " + uang;
        ret += "\nKebahagiaan\t: " + kebahagiaan;
        return ret;
    }

    public void meninggal() {
        if (!meninggal) {
            meninggal = true;
            System.out.println(nama + " meninggal dengan tenang, kebahagiaan : " + kebahagiaan);

            manusiaTerakhir.uang += uang;
            uang = 0;

            if (manusiaTerakhir!=this) {
                System.out.println("Semua harta " + nama + " disumbangkan untuk " + manusiaTerakhir.nama);
            }
            else {
                System.out.println("Semua harta " + nama + " hangus");
            }

        }
        else {
            System.out.println(nama + " telah tiada");
        }
    }

    public void tambahKebahagiaan(double jumlah) {
        kebahagiaan += jumlah;
        if (kebahagiaan > MAX_KEBAHAGIAAN) {
            kebahagiaan = MAX_KEBAHAGIAAN;
        }
    }

    public void kurangKebahagiaan(double jumlah) {
        kebahagiaan -= jumlah;
        if (kebahagiaan < MIN_KEBAHAGIAAN) {
            kebahagiaan = MIN_KEBAHAGIAAN;
        }
    }

    public boolean isRupawan() {
        return true;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getUmur() {
        return umur;
    }

    public void setUmur(int umur) {
        this.umur = umur;
    }

    public int getUang() {
        return uang;
    }

    public void setUang(int uang) {
        this.uang = uang;
    }

    public float getKebahagiaan() {
        return kebahagiaan;
    }

    public void setKebahagiaan(float kebahagiaan) {
        this.kebahagiaan = kebahagiaan;
    }
}
