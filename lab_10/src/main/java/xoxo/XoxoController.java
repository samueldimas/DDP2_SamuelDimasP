package xoxo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.swing.JOptionPane;

import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;
import xoxo.exceptions.InvalidCharacterException;
import xoxo.exceptions.KeyTooLongException;
import xoxo.exceptions.RangeExceededException;
import xoxo.exceptions.SizeTooBigException;
import xoxo.key.HugKey;
import xoxo.key.KissKey;
import xoxo.util.XoxoMessage;

/**
 * This class controls all the business
 * process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Samuel Dimas Partogi
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        gui.setDecryptFunction(new ActionListener() {
       
            @Override 
            public void actionPerformed(ActionEvent e) {
                decrypt();
           }
       });

        gui.setEncryptFunction(new ActionListener() {
       
           @Override
            public void actionPerformed(ActionEvent e) {
                encrypt();
           }
       });
    }

    /**
     * This method runs all of the encryption process needed.
     */
    public void encrypt() {
        if (gui.getMessageText().equals("") || gui.getSeedText().equals("")) {
            JOptionPane.showMessageDialog(null, "Message can't be empty");
        } else {
            try {
                XoxoEncryption xe = new XoxoEncryption(gui.getKeyText());
                try {
                    XoxoMessage xm = xe.encrypt(gui.getMessageText(), Integer.parseInt(gui.getSeedText()));
                    try {
                        FileWriter fw = new FileWriter("encrypt.enc");
                        fw.write(xm.getEncryptedMessage());
                        fw.flush();
                        fw.close();
                        gui.appendLog("Encryption success");
                    } catch (Exception e) {
                        e.printStackTrace();
                        gui.appendLog("Encryption failed");
                    }
                } catch (SizeTooBigException e) {
                    JOptionPane.showMessageDialog(null, "The size of this message is too big");
                    gui.appendLog("Encryption failed");
                } catch (InvalidCharacterException e) {
                    JOptionPane.showMessageDialog(null, "There are invalid characters in the message");
                    gui.appendLog("Encryption failed");
                }
            } catch (KeyTooLongException e) {
                JOptionPane.showMessageDialog(null, "The length of the key should be no more than 28 characters");
                gui.appendLog("Encryption failed");
            } catch (RangeExceededException e) {
                JOptionPane.showMessageDialog(null, "The seed range exceeds the allowed range");
            } 
        } 
    }

    /**
     * This method runs all of the decryption process needed.
     */
    public void decrypt() {
        String decrypted;
        if (gui.getMessageText().equals("") || gui.getSeedText().equals("")) {
            JOptionPane.showMessageDialog(null, "Message can't be empty");
        } else {
            try {
                XoxoDecryption xd = new XoxoDecryption(gui.getKeyText());
                try {
                    if (gui.getKeyText().equals("")) {
                        decrypted = xd.decrypt(gui.getKeyText(), HugKey.DEFAULT_SEED);
                    } else {
                        decrypted = xd.decrypt(gui.getMessageText(), Integer.parseInt(gui.getSeedText()));
                    }
                    try {
                        FileWriter fw = new FileWriter("decrypt.txt");
                        fw.write(decrypted);
                        fw.flush();
                        fw.close();
                        gui.appendLog("Encryption success");
                    } catch (Exception e) {
                        e.printStackTrace();
                        gui.appendLog("Encryption failed");
                    }
                } catch (SizeTooBigException e) {
                    JOptionPane.showMessageDialog(null, "The size of this message is too big");
                    gui.appendLog("Encryption failed");
                } catch (InvalidCharacterException e) {
                    JOptionPane.showMessageDialog(null, "There are invalid characters in the message");
                    gui.appendLog("Encryption failed");
                }  
            } catch (KeyTooLongException e) {
                JOptionPane.showMessageDialog(null, "The length of the key should be no more than 28 characters");
                gui.appendLog("Encryption failed");
            } catch (RangeExceededException e) {
                JOptionPane.showMessageDialog(null, "The seed range exceeds the allowed range");
            } 
        }
    }
}