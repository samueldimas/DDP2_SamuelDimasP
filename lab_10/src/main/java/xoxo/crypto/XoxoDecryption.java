package xoxo.crypto;

import java.io.UnsupportedEncodingException;
import xoxo.exceptions.InvalidCharacterException;
import xoxo.exceptions.KeyTooLongException;
import xoxo.exceptions.RangeExceededException;
import xoxo.exceptions.SizeTooBigException;
import xoxo.key.KissKey;

/**
 * This class is used to create a decryption instance
 * that can be used to decrypt an encrypted message.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Samuel Dimas Partogi
 */
public class XoxoDecryption {

    /**
     * A Hug Key string that is required to decrypt the message.
     */
    private String hugKeyString;

    /**
     * Class constructor with the given Hug Key string.
     */
    public XoxoDecryption(String hugKeyString) throws KeyTooLongException {
        this.hugKeyString = hugKeyString;
    }

    public int keyAt(int i) {
        return hugKeyString.charAt(i % hugKeyString.length());
    }

    /**
     * Decrypts an encrypted message.
     * 
     * @param encryptedMessage An encrypted message that wants to be decrypted.
     * @return The original message before it is encrypted.
     */
    public String decrypt(String encryptedMessage, int seed) {
        String result = "";
        byte[] byteArray;

        try {
            byteArray = encryptedMessage.getBytes("UTF-16BE");
        } catch (UnsupportedEncodingException e) {
            JOptionPane.showMessageDialog(null, "Unsupported encoding");
        }

        if (encryptedMessage.getBytes("UTF-16BE").length * 8 > 10000) {
            throw new SizeTooBigException("The size of message should not be more than 10 Kbits");
        } else {
            if (encryptedMessage.matches("[a-zA-Z]") || encryptedMessage.matches("[@]")) {
                for (int i = 0; i < encryptedMessage.length(); i++) {
                    int a = hugKeyString.charAt(i % hugKeyString.length());
                    int b = a ^ seed;
                    b -= 'a';
                    int d = encryptedMessage.charAt(i) ^ b;
                    result += (char) d;
                }
            } else {
                throw new InvalidCharacterException("There are characters unallowed in this message");
            }
        }

        return result;
    }
}