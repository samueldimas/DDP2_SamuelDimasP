package xoxo.crypto;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

import xoxo.exceptions.InvalidCharacterException;
import xoxo.exceptions.KeyTooLongException;
import xoxo.exceptions.RangeExceededException;
import xoxo.exceptions.SizeTooBigException;
import xoxo.key.HugKey;
import xoxo.key.KissKey;
import xoxo.util.XoxoMessage;

/**
 * This class is used to create an encryption instance
 * that can be used to encrypt a plain text message.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Samuel Dimas Partogi
 */
public class XoxoEncryption {

    /**
     * A Kiss Key object that is required to encrypt the message.
     */
    private KissKey kissKey;

    /**
     * Class constructor with the given Kiss Key
     * string to build the Kiss Key object.
     * 
     * @throws KeyTooLongException if the length of the
     *         kissKeyString exceeded 28 characters.
     */
    public XoxoEncryption(String kissKeyString) throws KeyTooLongException {

        if (kissKeyString.length() > 28) {
            throw new KeyTooLongException("The length of the key should not exceed 28 characters");
        }

        this.kissKey = new KissKey(kissKeyString);
    }

    /**
     * Encrypts a message in order to make it unreadable.
     * 
     * @param message The message that wants to be encrypted.
     * @return A XoxoMessage object that contains the encrypted message
     *         and a Hug Key object that can be used to decrypt the message.
     */
    public XoxoMessage encrypt(String message) throws IOException {
        String encryptedMessage = this.encryptMessage(message); 
        return new XoxoMessage(encryptedMessage, new HugKey(this.kissKey));
    }

    /**
     * Encrypts a message in order to make it unreadable.
     * 
     * @param message The message that wants to be encrypted.
     * @param seed A number to generate different Hug Key.
     * @return A XoxoMessage object that contains the encrypted message
     *         and a Hug Key object that can be used to decrypt the message.
     * @throws RangeExceededException if the seed range exceeds the allowed range.
     */
    public XoxoMessage encrypt(String message, int seed) {
        if (seed >= 0 && seed <= 36) {
            String encryptedMessage = this.encryptMessage(message); 
            return new XoxoMessage(encryptedMessage, new HugKey(this.kissKey, seed));
        } else {
            throw new RangeExceededException("The seed range exceeds the allowed range");
        }
    }

    /**
     * Runs the encryption algorithm to turn the message string
     * into an encrypted message string.
     * 
     * @param message The message that wants to be encrypted.
     * @return The encrypted message string.
     * @throws SizeTooBigException if the message size is more than 10 Kilobits.
     * @throws InvalidCharacterException if the message characters doesn't meet the requirements.
     */
    private String encryptMessage(String message) {
        final int length = message.length();
        String encryptedMessage = "";
        byte[] byteArray;

        try {
            byteArray = message.getBytes("UTF-16BE");
        } catch (UnsupportedEncodingException e) {
            JOptionPane.showMessageDialog(null, "Unsupported encoding");
        }

        if (message.getBytes("UTF-16BE").length * 8 > 10000) {
            throw new SizeTooBigException("The size of message should not be more than 10 Kbits");
        } else {
            if (message.matches("[a-zA-Z]") || message.matches("[@]")) {
                for (int i = 0; i < length; i++) {
                    int m = message.charAt(i);
                    int k = this.kissKey.keyAt(i) - 'a';
                    int value = m ^ k;
                    encryptedMessage += (char) value;
                }
            } else {
                throw new InvalidCharacterException("There are characters unallowed in this message");
            }
        }
        
        return encryptedMessage;

    }
}

