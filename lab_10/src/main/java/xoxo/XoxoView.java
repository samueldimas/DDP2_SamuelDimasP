package xoxo;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.JTextArea;

/**
 * This class handles most of the GUI construction.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Samuel Dimas Partogi
 */
public class XoxoView {
    
    /**
     * A frame that to place every GUI components.
     */
    private JFrame frame;

    /**
     * A label that labels the message text field.
     */
    private JLabel messageLabel;

    /**
     * A field that is used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A label that labels the key field.
     */
    private JLabel keyLabel;

    /**
     * A field that is used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;

    /**
     * A label that labels the seed field.
     */
    private JLabel seedLabel;

    /**
     * A field to be the input of the seed.
     */
    private JTextField seedField;

    /**
     * A label that labels the log field.
     */
    private JLabel logLabel;

    /**
     * A field that is used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField; 

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;

    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }

    /**
     * Constructs the GUI.
     */
    private void initGui() {
        frame = new JFrame();
        frame.setTitle("Xoxo");
        frame.setLayout(new BorderLayout());
        
        JPanel panel1 = new JPanel(new GridLayout(3, 3));

        messageLabel = new JLabel("Message    :    "); 
        messageLabel.setFont(new Font("Times", Font.BOLD, 18));
        messageField = new JTextField();
        messageField.setFont(new Font("Times", Font.BOLD, 18));
        keyLabel = new JLabel("Key             :    ");
        keyLabel.setFont(new Font("Times", Font.BOLD, 18));
        keyField = new JTextField();
        keyField.setFont(new Font("Times", Font.BOLD, 18));
        seedLabel = new JLabel("Seed           :    ");
        seedLabel.setFont(new Font("Times", Font.BOLD, 18));
        seedField = new JTextField();
        seedField.setFont(new Font("Times", Font.BOLD, 18));

        panel1.add(messageLabel);
        panel1.add(messageField);
        panel1.add(keyLabel);
        panel1.add(keyField);
        panel1.add(seedLabel);
        panel1.add(seedField);

        JPanel panel2 = new JPanel(new FlowLayout());

        encryptButton = new JButton("Encrypt");
        encryptButton.setFont(new Font("Times", Font.BOLD, 18));
        decryptButton = new JButton("Decrypt");
        decryptButton.setFont(new Font("Times", Font.BOLD, 18));

        panel2.add(encryptButton);
        panel2.add(decryptButton);
         
        JPanel panel3 = new JPanel(new GridLayout(2, 1));

        logLabel = new JLabel("Log:");
        logLabel.setFont(new Font("Times", Font.BOLD, 18));
        logField = new JTextArea();
        logField.setFont(new Font("Times", Font.BOLD, 18));
    
        panel3.add(logLabel);
        panel3.add(logField);

        frame.add(panel1, BorderLayout.LINE_START);
        frame.add(panel2, BorderLayout.CENTER);
        frame.add(panel3, BorderLayout.PAGE_END);
        

        frame.setLocation(500, 150);
        frame.setPreferredSize(new Dimension(500,200));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Gets the message from the message field.
     * 
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     * 
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Gets the seed text from the key field.
     * 
     * @return The input key string.
     */
    public String getSeedText() {
        return seedField.getText();
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    public JTextArea getLogField() {
        return logField;
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }
    
    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }
}