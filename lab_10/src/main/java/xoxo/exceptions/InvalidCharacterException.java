package xoxo.exceptions;

/**
 * An exception that is thrown if the message
 * doesn't meet the chacarters requirement.
 * 
 * @author Samuel Dimas Partogi.
 */
public class InvalidCharacterException extends RuntimeException {

    /**
     * Defines the constructor for 
     * {@code InvalidCharacterException}
     */
    public InvalidCharacterException(String message) {
        super(message);
    }

}