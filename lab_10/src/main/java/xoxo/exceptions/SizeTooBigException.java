package xoxo.exceptions;

/**
 * An exception that is thrown if the message 
 * size is more than 10 Kilo bits.
 * 
 * @author Samuel Dimas Partogi
 */
public class SizeTooBigException extends RuntimeException {              

    /**
     * Defines the constructor for 
     * {@code SizeTooBigException}
     */
    public SizeTooBigException(String message) {
        super(message);
    }

}