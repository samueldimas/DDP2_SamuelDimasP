package xoxo.exceptions;

/**
 * An exception that is thrown if the seed range
 * exceeds the allowed range.
 * 
 * @author Samuel Dimas Partogi
 */
public class RangeExceededException extends RuntimeException {


    /**
     * Defines the constructor for 
     * {@code RangeExceededException}
     */
    public RangeExceededException(String message) {
        super(message);
    }

}