import java.util.ArrayList;

public class Staff extends Karyawan {

    private ArrayList<Karyawan> dataIntern;

    public Staff(String nama, String tipe, int gaji) {
        this.nama = nama;
        this.tipe = tipe;
        this.gaji = gaji;
        this.gajiCount = 0;
        this.dataIntern = new ArrayList<>();
    }

    public ArrayList<Karyawan> getDataIntern() {
        return dataIntern;
    }
    
}