import java.util.Scanner;

public class Lab8 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Korporasi siangTampan = new Korporasi("PT. TAMPAN");
        boolean bool = true;

        while (bool) {
            String input = in.nextLine();
            String[] inputSplitted = input.split(" "); 
            if (inputSplitted[0].equals("TAMBAH_KARYAWAN")) {
                System.out.println(siangTampan.tambah(inputSplitted[1], inputSplitted[2], Integer.parseInt(inputSplitted[3])));
            } else if (inputSplitted[0].equals("STATUS")) {
                System.out.println(siangTampan.status(inputSplitted[1]));
            } else if (inputSplitted[0].equals("GAJIAN")) {
                System.out.println(siangTampan.gajian());
            } else if (inputSplitted[0].equals("TAMBAH_BAWAHAN")) {
                System.out.println(siangTampan.rekrut(inputSplitted[1], inputSplitted[2]));
            } else if (inputSplitted[0].equals("STOP")) {
                bool = false;
            } else if (isInteger(inputSplitted[0])) {
                siangTampan.setGajiMinimal(Integer.parseInt(inputSplitted[0]));
            } else {
                System.out.println("Input yang anda berikan tidak valid");
            }
        }
    }

    public static boolean isInteger(String integer) {
        try {
            Integer.parseInt(integer);
        } catch (NumberFormatException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }

        return true;
    }
}
