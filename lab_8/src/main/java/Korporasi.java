import java.util.ArrayList;

public class Korporasi {

    private String namaKorporasi;
    private ArrayList<Karyawan> data;
    private int gajiMinimal;

    public Korporasi(String namaKorporasi) {
        this.namaKorporasi = namaKorporasi;
        this.data = new ArrayList<>();
        this.gajiMinimal = 18000;
    }

    public String getNamaKorporasi() {
        return namaKorporasi;
    }
    
    public ArrayList<Karyawan> getData() {
        return data;
    }

    public int getGajiMinimal() {
        return gajiMinimal;
    }

    public void setGajiMinimal(int gajiMinimal) {
        this.gajiMinimal = gajiMinimal;
    }

    public Karyawan find(String nama) {
        Karyawan result = null;

        for (Karyawan karyawan : data) {
            if (karyawan.getNama().equals(nama)) {
                result = karyawan;
                break;
            }
        }
        return result;
    }

    public Manager findManager(String nama) {
        Manager result = null;

        for (Karyawan karyawan : data) {
            if (karyawan.getNama().equals(nama)) {
                result = (Manager) karyawan;
                break;
            }
        }

        return result;
    }

    public Staff findStaff(String nama) {
        Staff result = null;

        for (Karyawan karyawan : data) {
            if (karyawan.getNama().equals(nama)) {
                result = (Staff) karyawan;
                break;
            }
        }

        return result;
    }

    public String tambah(String namaKaryawan, String tipe, int gaji) {
        String result = "";
        Karyawan karyawan = find(namaKaryawan);

        if (karyawan != null) {
            result = "Karyawan dengan nama " + namaKaryawan + " telah terdaftar";
        } else {
            if (tipe.equals("MANAGER")) {
                data.add(new Manager(namaKaryawan, tipe, gaji));
            } else if (tipe.equals("STAFF")) {
                data.add(new Staff(namaKaryawan, tipe, gaji));
            } else {
                data.add(new Intern(namaKaryawan, tipe, gaji));
            }
            result = namaKaryawan + " mulai bekerja sebagai " + tipe +  " di " + namaKorporasi;
        }
        return result;
    }

    public String status(String nama) {
        String result = "";
        Karyawan karyawan = find(nama);

        if (karyawan != null) {
            result = karyawan.getNama().substring(0,1) 
                + karyawan.getNama().substring(1) + " " + karyawan.getGaji();
        } else {
            result = "Karyawan tidak ditemukan";
        }

        return result;
    }

    public String rekrut(String namaAtasan, String namaBawahan) {
        String result = "";
        Karyawan atasan = find(namaAtasan);
        Karyawan bawahan = find(namaBawahan);
        
        if (atasan != null) {
            if (bawahan != null) {
                if (atasan.getTipe().equals("MANAGER")) {
                    Manager atasan1 = findManager(namaAtasan);
                    if (atasan1.getDataStaff().size() == 10) {
                        result = "Anda tidak dapat merekrut bawahan lagi";
                    } else {
                        if (bawahan.getTipe().equals("MANAGER")) {
                            result = namaAtasan + " tidak dapat merekrut " + namaBawahan + " sebagai bawahan";
                        } else {
                            atasan1.getDataStaff().add(bawahan);
                            result = "Karyawan " + namaBawahan + " berhasil ditambahkan menjadi bawhaan " + namaAtasan;
                        }
                    }
                } else if (atasan.getTipe().equals("STAFF")) {
                    Staff atasan2 = findStaff(namaAtasan);
                    if (bawahan.getTipe().equals("MANAGER") || bawahan.getTipe().equals("STAFF")) {
                        result = "Anda tidak layak memiliki bawahan";
                    } else {
                        if (atasan2.getDataIntern().size() == 10) {
                            result = "Anda tidak dapat merekrut bawahan lagi";
                        } else {
                            atasan2.getDataIntern().add(bawahan);
                            result = "Karyawan " + namaBawahan + " berhasil ditambahkan menjadi bawhaan " + namaAtasan;
                        }
                    }
                } else {
                    result = "Anda tidak layak memiliki bawahan";
                }
            } else {
                result = "Karyawan tidak ditemukan";
            }
        } else {
            result = "Karyawan tidak ditemukan";
        }

        return result;
    }

    public String gajian() {
        String result = "";

        for (Karyawan karyawan : data) {
            karyawan.setGajiCount(karyawan.getGajiCount() + 1);  
        }
        result += "Semua karyawan telah diberikan gaji";

        for (Karyawan karyawan : data) {
            if (karyawan.getGajiCount() % 6 == 0) {
                int gajiTemp = karyawan.getGaji();
                karyawan.setGaji(karyawan.getGaji() + ((int)(karyawan.getGaji() * 0.1)));
                result += "\n" + karyawan.getNama() + " mengalami kenaikan gaji sebesar 10% dari " 
                    + gajiTemp + " menjadi " + karyawan.getGaji();
            }
        }

        for (Karyawan karyawan : data) {
            if (karyawan.getTipe().equals("STAFF") && karyawan.getGaji() >= getGajiMinimal()) {
                String namaTemp = karyawan.getNama();
                int gajiTemp = karyawan.getGaji();

                data.remove(karyawan);
                Manager tempManager = new Manager(namaTemp, "MANAGER", gajiTemp);
                data.add(tempManager);

                result += "\nSelamat, " + tempManager.getNama() + " telah dipromosikan menjadi MANAGER";
            }
        }
        
        return result;
    }
}