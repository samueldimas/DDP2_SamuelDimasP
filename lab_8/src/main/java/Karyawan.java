public abstract class Karyawan {

    protected String nama;
    protected String tipe;
    protected int gaji;
    protected int gajiCount;

    public String getNama() {
        return nama;
    }

    public String getTipe() {
        return tipe;
    }

    public int getGaji() {
        return gaji;
    }

    public void setGaji(int gaji) {
        this.gaji = gaji;
    }

    public int getGajiCount() {
        return gajiCount;
    }

    public void setGajiCount(int gajiCount) {
        this.gajiCount = gajiCount;
    }
    
}