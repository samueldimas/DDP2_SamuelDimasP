import java.util.ArrayList;

public class Manager extends Karyawan {

    private ArrayList<Karyawan> dataStaff;

    public Manager(String nama, String tipe, int gaji) {
        this.nama = nama;
        this.tipe = tipe;
        this.gaji = gaji;
        this.gajiCount = 0;
        this.dataStaff = new ArrayList<>();
    }

    public ArrayList<Karyawan> getDataStaff() {
        return dataStaff;
    }

}