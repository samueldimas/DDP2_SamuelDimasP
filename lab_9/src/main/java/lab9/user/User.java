package lab9.user;

import lab9.event.Event;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.lang.Comparable;

/**
* Class representing a user, willing to attend event(s).
*/
public class User {

    /** Name of user */
    private String name;
    
    /** List of events this user plans to attend  */
    private ArrayList<Event> events;
    
    private ArrayList<Event> temp = new ArrayList<>();

    /** 
     * Defines the constructor for
     * {@code User}.
     * 
     * Initializes a user object with given name and empty event list.
     */
    public User(String name) {
        this.name = name;
        this.events = new ArrayList<>();
    }
    
    /**
    * Accessor for name field.
    * @return name of this instance.
    */
    public String getName() {
        return name;
    }

    /**
    * Adds a new event to this user's planned events, if not overlapping
    * with currently planned events.
    *
    * @return true if the event if successfully added, false otherwise.
    */
    public boolean addEvent(Event newEvent) {
        boolean result = true;

        for (Event event : events) {
            if (event.overlapsWith(newEvent)) {
                result = false;
                break;
            } else {
                events.add(newEvent);
            }
        }

        return result;
    }

    /**
    * Returns the list of events this user plans to attend,
    * Sorted by their starting time.
    * Note: The list returned from this method is a copy of the actual
    *       events field, to avoid mutation from external sources
    *
    * @return list of events this user plans to attend
    */
    public ArrayList<Event> getEvents() {
        for (Event e : events) {
            temp.add(e);
        }

        Collections.sort(temp);
        return temp;
    }

    /**
     * Counts the total cost a user is attending.
     * 
     * @return total cost in BigInteger.
     */
    public BigInteger getTotalCost() {
        BigInteger result = new BigInteger("0");

        for (Event event : getEvents()) {
             result = result.add(event.getCost());
        }

        return result;
    }
}


