package lab9;

import lab9.user.User;
import lab9.event.Event;

import java.util.ArrayList;

/**
* Class representing event managing system
*/
public class EventSystem
{
    /**
    * List of events
    */
    private ArrayList<Event> events;
    
    /**
    * List of users
    */
    private ArrayList<User> users;
    
    /**
    * Constructor. Initializes events and users with empty lists.
    */
    public EventSystem() {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    /**
     * Finds the event according to its name
     * in the event system.
     * 
     * @param nameOfEvent name of the event.
     * @return object Event with associated name.
     */
    public Event findEvent(String nameOfEvent) {
        Event result = null;

        for (Event event : events) {
            if (event.getName().equals(nameOfEvent)) {
                result = event;
                break;
            }
        }

        return result;
    }

    /**
     * Returns the detail of an event
     * according to its name.
     * 
     * @param nameOfEvent name of the event.
     * @return String of details of the event (if exists).
     */
    public String getEvent(String nameOfEvent) {
        String result = null;

        Event event = findEvent(nameOfEvent);

        if (event == null) {
            result = "Event " + nameOfEvent + " tidak ada";
        } else {
            result = event.toString();
        }

        return result;
    }

    /**
     * Finds the user in the event system
     * according to its name.
     * 
     * @param nameOfUser name of the user
     * @return object User (if exists).
     */
    public User getUser(String nameOfUser) {
        User result = null;

        for (User user : users) {
            if (user.getName().equals(nameOfUser)) {
                result = user;
                break;
            }
        }

        return result;
    }

    /**
     * Adds an event to the event system.
     * 
     * @param name name of the event.
     * @param startTimeStr start time of the event.
     * @param endTimeStr end time of the event.
     * @param costPerHourStr cost per hour of the event.
     * @return details of whether an event is successfully added or not.
     */
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr) {
        Event event = findEvent(name);
        String result = null;
        Event temp;

        if (event != null) {
            result = "Event " + name + " sudah ada!";
        } else {
            temp = new Event(name, startTimeStr, endTimeStr, costPerHourStr);
            if (temp.getEnd().compareTo(temp.getStart()) == -1) {
                result = "Waktu yang diinputkan tidak valid";
            } else {
                events.add(temp);
                result = "Event " + name + " berhasil ditambahkan!";
            }
        }
        
        return result;
    }
    
    /** 
     * Adds the user to the event system.
     * 
     * @param name the name of the user
     * @return String of details if the user is
     * successfully added or not.
     */
    public String addUser(String name) {
        User user = getUser(name);
        String result = null;
        
        if (user != null) {
            result = "User " + name + " sudah ada!";
        } else {
            users.add(new User(name));
            result = "User " + name + " berhasil ditambahkan!";
        }

        return result;
    }
    
    /**
     * Registers the user to event available
     * considering the conditions required.
     * 
     * @param userName name of the user to be assigned to an event.
     * @param eventName name of the event to be assigned by a user.
     * @return a string that indicates each conditions met.
     */
    public String registerToEvent(String userName, String eventName) {
        String result = null;
        boolean notExists = true;
        
        Event now = findEvent(eventName);
        User then = getUser(userName);

        if (now == null) {
            result = "Tidak ada acara dengan nama "+ eventName + "!";
        } else if ((now == null) & (then == null)) {
            result = "Tidak ada pengguna dengan nama  "+ userName 
                + " dan acara dengan nama " + eventName + "!";
        } else if (then == null) {
            result = "Tidak ada pengguna dengan nama "+ userName + "!";
        } else {

            for (Event e : events) {
                if (!e.overlapsWith(now)) {
                    result = userName + "sibuk sehingga tidak dapat menghadiri " + eventName + "!";
                    notExists = false;
                    break;
                } else {
                    continue;
                }
            }

            if (notExists) {
                then.getEvents().add(now);
                result = userName + " berencana menghadiri " + eventName + "!";
            } else {
                result = userName + "sibuk sehingga tidak dapat menghadiri " + eventName + "!";
            }

        }

        return result;
    }
}