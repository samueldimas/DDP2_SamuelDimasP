package lab9.event;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
* A class representing an event and its properties
*/
public class Event implements Comparable<Event> {
    /** Name of event. */
    private String name;
    
    /** Start time of the event. */
    private GregorianCalendar start;

    /** End time of the event. */
    private GregorianCalendar end;

    /** Cost per hour. */
    private BigInteger bi;

    /** 
     * Defines the constructor for
     * {@code Event}
     * 
     * (Note that GregorianCalendar month is zero-based).
     */
    public Event(String name, String start, String end, String cost) {
        this.name = name;
        String[] startArray = start.split("_");
        String[] startDateArray = startArray[0].split("-");
        String[] startTimeArray = startArray[1].split(":");
        this.start = new GregorianCalendar(Integer.parseInt(startDateArray[0]), 
            Integer.parseInt(startDateArray[1]) - 1, Integer.parseInt(startDateArray[2]), 
            Integer.parseInt(startTimeArray[0]), Integer.parseInt(startTimeArray[1]), 
            Integer.parseInt(startTimeArray[2]));
        String[] endArray = end.split("_");
        String[] endDateArray = endArray[0].split("-");
        String[] endTimeArray = endArray[1].split(":");
        this.end = new GregorianCalendar(Integer.parseInt(endDateArray[0]), 
            Integer.parseInt(endDateArray[1]) - 1, Integer.parseInt(endDateArray[2]), 
            Integer.parseInt(endTimeArray[0]), Integer.parseInt(endTimeArray[1]), 
            Integer.parseInt(endTimeArray[2]));
        this.bi = new BigInteger(cost);
    }

    /**
    * Accessor for name field. 
    * @return name of this event instance
    */
    public String getName() {
        return name;
    }

    /**
    * Accessor for start field. 
    * @return start of this event instance.
    */
    public GregorianCalendar getStart() {
        return start;
    }

    /**
    * Accessor for end field. 
    * @return end of this event instance.
    */
    public GregorianCalendar getEnd() {
        return end;
    }

    /**
    * Accessor for bi field. 
    * @return bi of this event instance
    */
    public BigInteger getCost() {
        return bi;
    }

    /**
     * Checks if an event overlaps
     * with another event.
     * 
     * @param other event to be compared.
     * @return true if overlaps, else false.
     */
    public boolean overlapsWith(Event other) {
        return other.start.before(end) || start.before(other.end);
    }
    
    /**
     * Gives specific details of an event
     * (Note that GregorianCalendar month is 0-based).
     * 
     * @return String of event details.
     */
    public String toString() {
        String result;
        
        SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat time = new SimpleDateFormat("HH:mm:ss");
        String startDateFormat = date.format(start.getTime());
        String endDateFormat = date.format(end.getTime());
        String startTimeFormat = time.format(start.getTime());
        String endTimeFormat = time.format(end.getTime());

        result = getName() 
            + "\nWaktu mulai: " + startDateFormat + ", " + startTimeFormat 
            + "\nWaktu selesai: " + endDateFormat + ", " + endTimeFormat 
            + "\nBiaya kehadiran: " + getCost();

        return result;
    }

    /**
     * Compares this event with another event.
     * 
     * @param other event to be compared.
     * @return 0 if same time, 
     * positive integer if this event is after the other event,
     * negative integer if this event is before the other event 
     */
    @Override
    public int compareTo(Event other) {
        return getStart().compareTo(other.getStart());
    }
}
