package character;

import java.util.ArrayList;

//  write Player Class here
public class Player {

    private ArrayList<Player> diet;
    private boolean isCooked;
    private boolean isDead;
    private String name;
    private int hp;
    String type;
    String roar;
 
    public Player(String name, int hp) {
        this.name = name;
        this.hp = hp;
        this.diet = new ArrayList<>();
        this.isCooked = false;
        this.isDead = false;
    }

    public Player(String name, int hp, String roar) {
        this.name = name;
        this.hp = hp;
        this.diet = new ArrayList<>();
        this.isCooked = false;
        this.isDead = false;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        if (hp < 0) {
            this.hp = 0;
        } else {
            this.hp = hp;
        }
    }

    public boolean isCooked() {
        return isCooked;
    }

    public void setCooked(boolean isCooked) {
        this.isCooked = isCooked;
    }

    public boolean isDead() {
        return isDead;
    }
    
    public void setDead(boolean isDead) {
        this.isDead = isDead;
    }

    public String getRoar() {
        return roar;
    }

    public void setRoar(String roar) {
        this.roar = roar;
    }

    public ArrayList<Player> getDiet() {
        return diet;
    }

    public boolean canEat(Player enemy) {
        boolean result = false;
        
        if (this.diet.size() == 0) {
            if (this.type.equals("Monster")) {
                result = true;
            } else {
                if (enemy.type.equals("Monster") 
                    && enemy.hp == 0 && enemy.isCooked == true) {
                    result = true;
                }
            }
        }
        
        return result;
    }

    public void burn(Player enemy) {} 

    public void attack(Player enemy) {
        // enemy.setHp(enemy.getHp() - 10);
        
        // if (enemy.getHp() == 0) {
        //     enemy.setDead(true);
        // }

        if (enemy instanceof Magician) {
            if (this instanceof Magician) {
                enemy.setHp(enemy.getHp() - 20);
            }
        } else {
            enemy.setHp(enemy.getHp() - 10);
        }
    }
}