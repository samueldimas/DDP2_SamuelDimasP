package character;

//  write Magician Class here
public class Magician extends Human {
    
    public Magician(String name, int hp) {
        super(name, hp);
        this.type = "Magician";
    }

    public void burn(Player enemy) {
        if (enemy.getType().equals("Magician")) {
            enemy.setHp(enemy.getHp() - 20);
        } else {
            enemy.setHp(enemy.getHp() - 10);
        }

        if (enemy.getHp() == 0) {
            enemy.setDead(true);
            enemy.setCooked(true);
        }
    }
    
    public void attack(Player enemy) {
        if (enemy.getType().equals("Magician")) {
            enemy.setHp(enemy.getHp() - 20);
        } else {
            enemy.setHp(enemy.getHp() - 10);
        }

        if (enemy.getHp() == 0) {
            enemy.setDead(true);
        }
    }
}