package character;

public class Monster extends Player {
    
    public Monster(String name, int hp) {
        super(name, hp * 2);
        this.type = "Monster";
        this.roar = "AAAAAAaaaAAAAAaaaAAAAAA";
    }

    public Monster(String name, int hp, String roar) {
        super(name, hp);
        this.type = "Monster";
        this.roar = roar;
    }

    public String roar() {
        return roar;
    }
}