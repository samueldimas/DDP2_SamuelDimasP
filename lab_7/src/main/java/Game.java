import character.Human;
import character.Magician;
import character.Monster;
import character.Player;
import java.util.ArrayList;

public class Game {
    ArrayList<Player> player = new ArrayList<>();
    
    /**
     * Fungsi untuk mencari karakter.
     * @param  name nama karakter yang ingin dicari.
     * @return Player chara object karakter yang dicari, 
     *     return null apabila tidak ditemukan.
     */
    public Player find(String name) {
        Player result = null;

        for (Player character : player) {
            if (character.getName().equals(name)) {
                result = character;
                break;
            }
        }

        return result;
    }

    public String listOfDiet(String name) {
        String result = "";
        Player character = find(name);

        if (character != null) {
            if (character.getDiet().size() == 0) {
                result = "Belum memakan siapa siapa";
            } else {
                result += "Memakan " + character.getDiet().get(0).getType() 
                    + " " +  character.getDiet().get(0).getName();
            }
        }

        return result;
    }

    /**
     * Fungsi untuk menambahkan karakter ke dalam game.
     * @param chara nama karakter yang ingin ditambahkan.
     * @param tipe tipe dari karakter yang ingin ditambahkan 
     *     terdiri dari monster, magician dan human.
     * @param hp hp dari karakter yang ingin ditambahkan.
     * @return String result hasil keluaran dari penambahan karakter 
     *     contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game".
     */
    public String add(String chara, String tipe, int hp) {
        String result = "";
        Player character = find(chara);

        if (character != null) {
            result = "Sudah ada karakter bernama " + chara;
        } else {
            if (tipe.equals("Human")) {
                player.add(new Human(chara, hp));
            } else if (tipe.equals("Magician")) {
                player.add(new Magician(chara,hp));
            } else {
                player.add(new Monster(chara, hp));
            }
            result = chara + " ditambahkan ke dalam game";
        }
        
        return result;
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, 
     * roar hanya bisa dilakukan oleh monster.
     * @param chara nama karakter yang ingin ditambahkan.
     * @param tipe tipe dari karakter yang ingin ditambahkan 
     *     terdiri dari monster, magician dan human.
     * @param hp hp dari karakter yang ingin ditambahkan.
     * @param roar teriakan dari karakter.
     * @return String result hasil keluaran dari penambahan karakter 
     *     contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game".
     */
    public String add(String chara, String tipe, int hp, String roar) {
        String result = "";
        Player character = find(chara);
        
        if (tipe.equals("Human") || tipe.equals("Monster")) {
            result = "Tipe karakter" + tipe + "tidak dapat melakukan roar";
        } else {
            if (character != null) {
                result = "Sudah ada karakter bernama " + chara;
            } else {
                player.add(new Monster(chara, hp));
                result = chara + " ditambahkan ke dalam game";
            }
        }

        return result;
    }

    /**
     * Fungsi untuk menghapus character dari game.
     * @param chara character yang ingin dihapus.
     * @return String result hasil keluaran dari game.
     */
    public String remove(String chara) {
        String result = "";
        Player character = find(chara);
        
        if (character != null) {
            player.remove(character);
            result = chara + " dihapus dari game";
        } else {
            result = "Tidak ada " + chara;
        }

        return result;
    }

    /**
     * Fungsi untuk menampilkan status character dari game.
     * @param chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game.
     */
    public String status(String chara) {
        String diet = "";
        String result = "";
        Player character = find(chara);
        
        if (character != null) {
            diet = listOfDiet(chara);
            result = character.getType() + " " + character.getName()
                + "\nHP: " + character.getHp() 
                + (character.isDead() == false 
                ? "\nMasih hidup\n" : "\nSudah meninggal dengan damai\n")
                + diet; 
        } else {
            result = "Tidak ada " + chara;
        }

        return result;
    }

    /**
     * Fungsi untuk menampilkan semua status dari character yang berada di dalam game.
     * @return String result nama dari semua character, 
     *     format sesuai dengan deskripsi soal atau contoh output.
     */
    public String status() {
        String diet = "";
        String result = "";
        int i;
        
        for (i = 0; i < player.size(); i++) {
            if (player.get(i).getDiet().size() == 0) {
                diet = "Belum memakan siapa siapa";
            } else {
                diet = listOfDiet(player.get(i).getName()); 
            }
            result += player.get(i).getType() + " " + player.get(i).getName()
                + "\nHP: " + player.get(i).getHp() 
                + (player.get(i).isDead() == false 
                ? "\nMasih hidup\n" : "\nSudah meninggal dengan damai\n")
                + diet + (i == player.size() - 1 ? "" : "\n");
        }
        
        return result;
    }

    /**
     * Fungsi untuk menampilkan character-character yang dimakan oleh chara.
     * @param chara Player yang ingin ditampilkan seluruh history player yang dimakan.
     * @return String result hasil dari karakter yang dimakan oleh chara.
     */
    public String diet(String chara) {
        String result = "";
        Player character = find(chara);

        if (character != null) {
            result = listOfDiet(chara);
        } else {
            result = "Tidak ada " + chara;
        }
        
        return result;
    }

    /**
     * Fungsi helper untuk memberikan list character yang dimakan dalam satu game.
     * @return String result hasil dari karakter yang dimakan dalam 1 game.
     */
    public String diet() {
        String result = "";
        int i;

        for (i = 0; i < player.size(); i++) {
            result += player.get(i).getType() + " " 
                + player.get(i).getName() + "\n";

            if (player.get(i).getDiet().size() == 0) {
                result += "Belum memakan siapa siapa" 
                    + (i == player.size() - 1 ? "" : "\n");
            } else {
                result += listOfDiet(player.get(i).getName()) 
                    + (i == player.size() - 1 ? "" : "\n");
            }
        }

        return result;
    }

    /**
     * Fungsi untuk menampilkan hasil dari me vs enemyName.
     * @param meName nama dari character yang sedang dimainkan.
     * @param enemyName nama dari character yang akan di serang.
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal.
     */
    public String attack(String meName, String enemyName) {
        String result = "";
        Player character = find(meName);
        Player enemy = find(enemyName);

        if (character != null) {
            if (enemy != null) {
                character.attack(enemy);
                result = "Nyawa " + enemyName + " " + enemy.getHp();
            } else {
                result = "Tidak ada " + enemyName;
            }
        } else {
            result = "Tidak ada " + meName;
        }
        
        return result;
    }

    /**
    * Fungsi untuk menampilkan hasil dari me vs enemyName, 
    *     method ini hanya boleh dilakukan oleh magician.
    * @param meName nama dari character yang sedang dimainkan.
    * @param enemyName nama dari character yang akan di bakar.
    * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal.
    */
    public String burn(String meName, String enemyName) {
        String result = "";
        Player character = find(meName);
        Player enemy = find(enemyName);

        if (character != null) {
            if (character.getType().equals("Magician")) {
                if (enemy != null) {
                    character.burn(enemy);
                    if (enemy.getHp() == 0) {
                        enemy.setCooked(true);
                        result = "Nyawa " + enemyName + " " 
                            + enemy.getHp() + "\ndan matang"; 
                    } else {
                        result = "Nyawa " + enemyName + " " + enemy.getHp();
                    }
                } else {
                    result = "Tidak ada " + enemyName;
                }
            } else {
                result = enemyName + " tidak dapat melakukan burn";
            }
        } else {
            result = "Tidak ada " + meName;
        }

        return result;
    }

    /**
    * Fungsi untuk menampilkan hasil dari me vs enemyName. 
    *     enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal.
    * @param meName nama dari character yang sedang dimainkan.
    * @param enemyName nama dari character yang akan di makan.
    * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal.
    */
    public String eat(String meName, String enemyName) {
        String result = "";
        Player character = find(meName);
        Player enemy = find(enemyName);

        if (character != null) {
            if (character.getDiet().size() == 1) {
                result = meName + " tidak bisa memakan lagi";
            } else {
                if (enemy != null) {
                    if (character.canEat(enemy)) {
                        character.setHp(character.getHp() + 15);
                        character.getDiet().add(enemy);
                        player.remove(enemy);
                        result =  meName + " memakan " + enemyName
                            + "\nNyawa " + meName + " kini " + character.getHp();
                    } else {
                        result = meName + " tidak bisa memakan " + enemyName;
                    }
                } else {
                    result = "Tidak ada " + enemyName;
                }
            }
        } else {
            result = "Tidak ada " + meName;
        }

        return result;
    }

    /**
    * Fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
    * @param meName nama dari character yang akan berteriak.
    * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal.
    */
    public String roar(String meName) {
        String result = "";
        Player character = find(meName);

        if (character != null) {
            if (character.getType().equals("Monster")) {
                result = character.getRoar();
            } else {
                result = meName + " tidak bisa berteriak";
            }
        } else {
            result = "Tidak ada " + meName;
        }
        
        return result;
    }

}