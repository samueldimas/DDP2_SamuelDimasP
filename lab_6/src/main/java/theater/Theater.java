package theater;

import java.util.ArrayList;
import java.util.Arrays;
import movie.Movie;
import ticket.Ticket;

public class Theater {
	
	private String name;
	private int revenue;
	private static int totalRevenue;
	private ArrayList<Ticket> listOfTickets;
	private Movie[] listOfMovies;
	
	public Theater(String name, int revenue, ArrayList<Ticket> listOfTickets, Movie[] listOfMovies) {
		this.name = name;
		this.revenue = revenue;
		this.listOfTickets = listOfTickets;
		this.listOfMovies = listOfMovies;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setRevenue(int revenue) {
		this.revenue = revenue;
	}
	
	public int getRevenue() {
		return revenue;
	}
	
	public ArrayList<Ticket> getListOfTickets() {
		return listOfTickets;
	}
	
	public void setListOfTickets(ArrayList<Ticket> listOfTickets) {
		this.listOfTickets = listOfTickets;
	}
	
	public Movie[] getListOfMovies() {
		return listOfMovies;
	}
	
	public void setListOfMovies(Movie[] listOfMovies) {
		this.listOfMovies = listOfMovies;
	}
	
	public String printListOfMovies(Movie[] listOfMovies){
		String listMovies = "";

		for (int i = 0; i < getListOfMovies().length; i++) {
			if (i == getListOfMovies().length - 1) {
				listMovies += getListOfMovies()[i].getTitle();
			} else {
				listMovies += getListOfMovies()[i].getTitle() + ", ";
			}
		}
		return listMovies;
	}

	public void printInfo() {
		System.out.println("------------------------------------------------------------------ " +
						   "\r\nBioskop                 : " + getName() + 
						   "\r\nSaldo Kas               : " + getRevenue() + 
						   "\r\nJumlah tiket tersedia   : " + getListOfTickets().size() +
						   "\r\nDaftar Film tersedia    : " + printListOfMovies(getListOfMovies()) +
						   "\r\n------------------------------------------------------------------");

	}
	
	public static void printTotalRevenueEarned(Theater[] theaters) {
		int totalRevenue = 0;

		for (int i = 0; i < theaters.length; i++) {
			totalRevenue += theaters[i].getRevenue();
		}

		for (int j = 0; j < theaters.length; j++) {
			if (j == 0) {
				System.out.println(" Total uang yang dimiliki Koh Mas : Rp. " + totalRevenue +"\r\n" + 
				"------------------------------------------------------------------\r\n" + 
				"Bioskop         : " + theaters[j].getName() + "\r\n" + 
				"Saldo Kas       : Rp. " + theaters[j].getRevenue() +  "\r\n" + 
				" ");
			} else if(j == theaters.length - 1) {
				System.out.println("Bioskop         : " + theaters[j].getName() + "\r\n" + 
				"Saldo Kas       : Rp. " + theaters[j].getRevenue() +  "\r\n" + 
				"------------------------------------------------------------------");
			} else {
				System.out.println("Bioskop         : " + theaters[j].getName() + "\r\n" + 
				"Saldo Kas       : Rp. " + theaters[j].getRevenue() +  "\r\n" + 
				" ");
			}
		}
	}	
}