package customer;

import java.util.ArrayList;
import movie.Movie;
import theater.Theater;
import ticket.Ticket;

public class Customer {
	
	private String name;
	private String gender;
	private int age;
	
	public Customer(String name, String gender, int age) {
		this.name = name;
		this.gender = gender;
		this.age = age;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getGender() {
		return gender;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public int getAge() {
		return age;
	}
	
	public Ticket orderTicket(Theater theater, String title, String day, String type){
		ArrayList<Ticket> listOfTickets = theater.getListOfTickets();
		Ticket ticket = null;

		for (int i = 0; i < listOfTickets.size(); i++) {
			if (title.equals(listOfTickets.get(i).getTitle()) && day.equals(listOfTickets.get(i).getDay()) && type.equals(listOfTickets.get(i).getType())){
				if (getAge() >= listOfTickets.get(i).getMovie().getMinimumAge()) {
	 				System.out.println(getName() + " telah membeli tiket " + title + " jenis " + type +
					" di " + theater.getName() + " pada hari " + day + " seharga Rp. " + listOfTickets.get(i).getPrice());
	 			    theater.setRevenue(theater.getRevenue() + listOfTickets.get(i).getPrice());
				    return listOfTickets.get(i);
				} else {
					System.out.println(getName() + " masih belum cukup umur untuk menonton " + listOfTickets.get(i).getTitle() +
					" dengan rating " + listOfTickets.get(i).getMovie().getRating());
				}
			} else if (i == listOfTickets.size() - 1) {
				System.out.println("Tiket untuk film " + title + " jenis " +  type + " dengan jadwal " + day +
				" tidak tersedia di " + theater.getName());
			}
			
		}
		return ticket;
	}

	public void findMovie(Theater theater, String title) {
		for (int i = 0; i < theater.getListOfMovies().length; i++){
			if (title.equals(theater.getListOfMovies()[i].getTitle())){
				System.out.println(theater.getListOfMovies()[i]);
			} else if (i == theater.getListOfMovies().length - 1){
				System.out.println("Film " +  title + " yang dicari " + this.getName() + 
								   " tidak ada di bioskop " + theater.getName());
			}
		}
		
	}
}