package movie;

public class Movie {

	private String title, rating, genre, area;
	private int duration;
	private int minimumAge;
	
	public Movie(String title, String rating, int duration, String genre, String area) {
		this.title = title;
		this.rating = rating;
		this.duration = duration;
		this.genre = genre;
		this.area = area;

		if (this.rating.equals("Remaja")) {
			this.minimumAge = 13;
		} else if (this.rating.equals("Dewasa")) {
			this.minimumAge = 17;
		} else { 
			this.minimumAge = 0;
		}
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setRating(String rating) {
		this.rating = rating;
	}
	
	public String getRating() {
		return rating;
	}
	
	public void setDuration(int duration) {
		this.duration = duration;
	}
	
	public int getDuration() {
		return duration;
	}
	
	public void setGenre(String genre) {
		this.genre = genre;
	}
	
	public String getGenre() {
		return genre;
	}
	
	public String getArea() {
		return area;
	}
	
	public void setArea(String area) {
		this.area = area;
	}
	
	public int getMinimumAge() {
		return minimumAge;
	}
	
	public void setMinimumAge(int minimumAge) {
		this.minimumAge = minimumAge; 
	}
	
	public String toString() {
		return "------------------------------------------------------------------ " + 
		"\r\nJudul   : " + this.getTitle() +  
		"\r\nGenre   : " + this.getGenre() +  
		"\r\nDurasi  : " + this.getDuration() + " menit " + 
		"\r\nRating  : " + this.getRating() +  
		"\r\nJenis   : Film " + this.getArea() + 
		"\r\n------------------------------------------------------------------";
	}
	
}