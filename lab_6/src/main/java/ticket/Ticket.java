package ticket;

import movie.Movie;

public class Ticket {
	
	private Movie movie;
	private String title, day, type;
	private boolean is3Dimension;
	private int price;
	
	public Ticket(Movie movie, String day, boolean is3Dimension) {
		this.movie = movie;
		this.title = movie.getTitle();
		this.day = day;
		this.is3Dimension = is3Dimension;

		if (is3Dimension) {
			this.type = "3 Dimensi";
		} else {
			this.type = "Biasa";
		}
		
		if (this.day.equals("Sabtu") || this.day.equals("Minggu")) {
			if (is3Dimension) {
				this.price = 120000;
			} else {
				this.price = 100000;
			}
		} else {
			if (is3Dimension) {
				this.price = 72000;
			} else {
				this.price = 60000;
			}
		}
	}
	
	public void setMovie(Movie movie) {
		this.movie = movie;
	}
	
	public Movie getMovie() {
		return this.movie;
	}
	
	public void setTitle(String title) {
		movie.setTitle(title);
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setDay(String day) {
		this.day = day;
	}
	
	public String getDay() {
		return day;
	}
	
	public boolean is3Dimension() {
		return is3Dimension;
	}
	
	public void set3Dimension(boolean is3Dimension) {
		this.is3Dimension = is3Dimension;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getType() {
		return type;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}
	
	public int getPrice() {
		return price;
	}
	
	public String toString() {
		return  "------------------------------------------------------------------ " + 
		"\r\nFilm            : " + this.getTitle() + 
		"\r\nJadwal Tayang   : " + this.getDay() + 
		"\r\nJenis           : " + this.getType() + 
		"\r\n------------------------------------------------------------------";
	}
}