/**
 * Code Author (Mahasiswa)
 * @author Samuel Dimas Partogi, NPM 1706074915, Kelas DDP2-B, GitLab Account: samueldimas
 * Code Version: 1.2.1.2
 */

import java.util.Scanner;

public class BingoCard {

	private Number[][] numbers;
	private Number[] numberStates; 
	private boolean isBingo;
	
	public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
	}

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}	

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	public String markNum(int num){
		try {
			if (getNumberStates()[num].isChecked()) {
				return num + " sebelumnya sudah tersilang";
			} else {
				getNumberStates()[num].setChecked(true);
				checkBingo();
				return num + " tersilang";
			}
		} catch (NullPointerException e) {
			return "Kartu tidak memiliki angka " + num;
		}
	}	
	
	public String info() {
		String info = "";
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				info += "| ";
				if (getNumbers()[i][j].isChecked() == true) {
					info += "X ";
				} else {
					info += getNumbers()[i][j].getValue();
				}
				info += " ";
			}
			if (i != 4) {
				info += "|\n";
			} else {
				info += "|";
			}
		}
		return info;
	}
	
	public void restart() {
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				getNumbers()[i][j].setChecked(false);
			}
		}
		System.out.println("Mulligan!");
	}

	public void checkBingo() {
		int counterX = 0;
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				if (getNumbers()[i][j].isChecked()) {
					counterX += 1;
				} else {
					counterX = 0;
				}
				if (counterX == 5) {
					setBingo(true);
					break;
				}
			}
			if (counterX == 5) {
				setBingo(true);
				break;
			}
		}

		int counterY = 0;
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				if (getNumbers()[j][i].isChecked()) {
					counterY += 1;
				} else {
					counterY = 0;
				}
				if (counterY == 5) {
					setBingo(true);
					break;
				}
			}
			if (counterY == 5) {
				setBingo(true);
				break;
			}
		}

		int counterZLeft = 0;
		for (int i = 0; i < 5; i++) {
			if (getNumbers()[i][i].isChecked()) {
				counterZLeft += 1;
			} else {
				counterZLeft = 0;
			}
			if (counterZLeft == 5) {
				setBingo(true);
				break;
			}
		}

		int counterZRight = 0;
		for (int i = 0; i < 5; i++) {
			if (getNumbers()[i][4 - i].isChecked()) {
				counterZRight += 1;
			} else {
				counterZRight = 0;
			}
			if (counterZRight == 5) {
				setBingo(true);
				break;
			}
		}
	}
}
