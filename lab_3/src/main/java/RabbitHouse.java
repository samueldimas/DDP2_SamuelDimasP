/**
 * Code Author (Mahasiswa)
 * @author Samuel Dimas Partogi, NPM 1706074915, Kelas DDP2-B, GitLab Account: samueldimas
 * Code Version: 1.2.0
 */


import java.util.Scanner;

public class RabbitHouse {
    
    public static int jumlah(String kelinci) {
        
        int hasil = 1;
        
        if (kelinci.length() <= 1) {

            hasil *= 1;

        } else {

            hasil += (kelinci.length() * jumlah(kelinci.substring(1, kelinci.length())));
        }

        return hasil;
    }

    public static int palindrome(String kelinci) {

        int hasil = 1;

        StringBuilder hitung = new StringBuilder(kelinci);

        if (hitung.reverse().toString().equals(kelinci)) {

            return 0;

        }

        if (kelinci.length() == 2) {

            return 1;

        } else {
            
            for (int i = 0; i < kelinci.length(); i++){

                StringBuilder hapus = new StringBuilder(kelinci);
                hasil+= palindrome(hapus.deleteCharAt(i).toString());

            }
        }

        return hasil;

    }

    public static void main (String[] args) {
        
        Scanner masukan = new Scanner(System.in);

        String kondisi[] = masukan.nextLine().split(" ");
        String jenis = kondisi[0];
        String nama = kondisi[1];

        if (jenis.equals("normal")) {

            System.out.print(RabbitHouse.jumlah(nama));
            
        } else if (jenis.equals("palindrome")) {

            System.out.print(RabbitHouse.palindrome(nama));

        }

        masukan.close();

    }
}
